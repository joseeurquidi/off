function getGrid (height, width, level) {

	var x_grid = 0
	var y_grid = 0
	var cant = 0

	if ( level < 6 ) {
		x_grid = 3
		y_grid = 3
		cant = 4
	}else if ( level >= 6 && level < 15) {
		x_grid = 3
		y_grid = 4
		cant = 7
	}else if ( level >= 15 && level < 24) {
		x_grid = 5
		y_grid = 4
		cant = 9
	}else if ( level >= 24 && level < 35) {
		x_grid = 5
		y_grid = 5
		cant = 13
	}else if ( level >= 35 && level < 42) {
		x_grid = 5
		y_grid = 6
		cant = 16
	}else if ( level >= 42 && level < 51) {
		x_grid = 5
		y_grid = 7
		cant = 21
	}else if ( level >= 51 ) {
		x_grid = 7
		y_grid = 7
		cant = 26
	}

	// if ( width > height) {
	// 	var auz = x_grid
	// 	x_grid = y_grid
	// 	y_grid = aux
	// }

	return [x_grid, y_grid, cant]
}

function getOn( cant, ons ){
	var cants = Array( cant - 1 )

	for( var i = 0; i < ons; i++){
		cants[i] = 1
	}

	return shuffle( cants )
}

function getWindowsPos(x_grid, y_grid, width, height ){

	var add_x = width * 0.05
	var add_y = height * 0.05
	var posible_x = width * 0.9
	var posible_y = height * 0.95

	var square = 0
	var porcentage = 0
	if ( posible_x / x_grid < posible_y / y_grid) {
		square = posible_x / x_grid
		porcentage = square / 256
	} else{
		square = posible_y / y_grid
		porcentage = square / 256
	}

	var start_x = posible_x / 2 - square / 2 - ( square * Math.floor( x_grid / 2 ) )
	var start_y = posible_y - ( square * y_grid )
	var arr_pos = []

	for( var i = 0; i < y_grid; i++){
		aux_x = start_x
		for( var j = 0; j < x_grid; j++){
			arr_pos.push( [ aux_x + add_x, start_y + add_y] )
			aux_x += square
		}

		start_y += square
	}

	// for( var i = 0; i < x_grid; i++){
	// 	aux_y = start_y
	// 	for( var j = 0; j < y_grid; j++){


	// 		arr_pos.push( [ start_x + add_x, aux_y + add_y] )
	// 		aux_y += square
	// 	}

	// 	start_x += square
	// }

	return [arr_pos, porcentage]
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }

    return a
}
