//Juego principal
//http://www.color-hex.com/color-palette/7479
//http://www.color-hex.com/color-palette/4619
//https://color.adobe.com/es/Pistachio-color-theme-7919341/
//https://www.flaticon.com/free-icon/play-button_26025#term=play%20button&page=1&position=12
var height = window.innerHeight
var width = window.innerWidth
var game = new Phaser.Game(width * window.devicePixelRatio, height * window.devicePixelRatio, Phaser.CANVAS, 'gameDiv')
var level = 1

var mainState = {
	preload: function(){
		//Fondo
		game.stage.backgroundColor = "#131862"

		//focos
		game.load.image('lightOn', 'img/light-bulb-on.png')
		game.load.image('lightOff', 'img/light-bulb-off.png')

		game.load.image('top_left_off', 'img/top_left_off.png')
		game.load.image('top_middle_off', 'img/top_middle_off.png')
		game.load.image('top_right_off', 'img/top_right_off.png')
		game.load.image('bottom_left_off', 'img/bottom_left_off.png')
		game.load.image('bottom_middle_off', 'img/bottom_middle_off.png')
		game.load.image('bottom_right_off', 'img/bottom_right_off.png')

		game.load.image('top_left_on', 'img/top_left_on.png')
		game.load.image('top_middle_on', 'img/top_middle_on.png')
		game.load.image('top_right_on', 'img/top_right_on.png')
		game.load.image('bottom_left_on', 'img/bottom_left_on.png')
		game.load.image('bottom_middle_on', 'img/bottom_middle_on.png')
		game.load.image('bottom_right_on', 'img/bottom_right_on.png')

		game.load.image('door', 'img/door.png')

		//play
		game.load.image('play', 'img/play-button.png')
	},

	create: function(){

		this.score = 0
		var style = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
		this.start = false

		//Fisica necesatio
		game.physics.startSystem(Phaser.Physics.ARCADE)

		//Zombie
		this.on = this.game.add.group()
		this.on.enableBody = true
		this.on.createMultiple(200, 'lightOn')

		//Humano
		this.off = this.game.add.group()
		this.off.enableBody = true
		this.off.createMultiple(200, 'lightOff')

		this.startGame()

	},

	update: function(){

	},

	//reinicia el juego
	startGame: function(){

		this.start = game.add.sprite(width / 2, height / 2, 'play')
		this.start.anchor.setTo(0.5);
		this.start.scale.setTo(0.5, 0.5)
		this.start.inputEnabled = true

		this.start.events.onInputDown.add( function(e){

			this.start.kill()

			this.createBuilding()

		}, this)

	},

	createBuilding: function(){
		var pos = getGrid(height, width, level)
		var lightsOn = getOn( pos[0] * pos[1], pos[2] )

		var aux_array_pos = getWindowsPos( pos[0], pos[1], width, height)
		var array_pos = aux_array_pos[0]
		var porcentage = aux_array_pos[1]
		cc = 0
		for( var y = 0; y < pos[1]; y++){
			for( var x = 0; x < pos[0]; x++){

				var opt = undefined
				//Puerta
				if( y == pos[1] - 1 && x == Math.floor( pos[0] / 2 ) ){
					var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'door')

				//Ventana
				}else{
					opt = lightsOn.shift()

					//Esquina superior izquierda
					if ( x == 0 && y == 0) {
						if ( opt == 1 ) {
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'top_left_on')
							this_window.offSprite = "top_left_off"
							this_window.onSprite = "top_left_on"
						}else{
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'top_left_off')
						}

					//Esquina superior derecha
					}else if (y == 0 && x == pos[0] - 1) {
						if ( opt == 1 ) {
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'top_right_on')
							this_window.offSprite = "top_right_off"
							this_window.onSprite = "top_right_on"
						}else{
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'top_right_off')
						}

					//Techo
					}else if ( y == 0) {
						if ( opt == 1 ) {
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'top_middle_on')
							this_window.offSprite = "top_middle_off"
							this_window.onSprite = "top_middle_on"
						}else{
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'top_middle_off')
						}

					//Pared izquierda
					}else if ( x == 0) {
						if ( opt == 1 ) {
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'bottom_left_on')
							this_window.offSprite = "bottom_left_off"
							this_window.onSprite = "bottom_left_on"
						}else{
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'bottom_left_off')
						}

					//Pared derecha
					}else if ( x == pos[0] - 1) {
						if ( opt == 1 ) {
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'bottom_right_on')
							this_window.offSprite = "bottom_right_off"
							this_window.onSprite = "bottom_right_on"
						}else{
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'bottom_right_off')
						}

					//Centros
					}else{
						if ( opt == 1 ) {
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'bottom_middle_on')
							this_window.offSprite = "bottom_middle_off"
							this_window.onSprite = "bottom_middle_on"
						}else{
							var this_window = game.add.sprite(array_pos[cc][0], array_pos[cc][1], 'bottom_middle_off')
						}
					}

				}


				this_window.scale.setTo(porcentage, porcentage)

				if ( opt == 1 ) {
					this_window.inputEnabled = true

					this_window.events.onInputDown.add( function(e){

						e.loadTexture(e.offSprite, 0, false)
						game.time.events.add(Phaser.Timer.SECOND * 1, this.return_Original_sprite, this, e);
						e.events.onInputDown.removeAll()

					}, this)

				}


				cc += 1
			}
		}
	},

	return_Original_sprite: function( elem ){

		elem.loadTexture(elem.onSprite, 0, false)
		elem.events.onInputDown.add( function(e){

			e.loadTexture(e.offSprite, 0, false)
			game.time.events.add(Phaser.Timer.SECOND * 1, this.return_Original_sprite, this, e);
			e.events.onInputDown.removeAll()

		}, this)

	}
}

game.state.add('main', mainState)
game.state.start('main')
